//1st part
//---------------------------------------------------------

db.users.find(
	{	
		$or: [
			{firstName: {$regex: 's', $regex: 'S'}},
			{lastName: {$regex: 'd',  $regex: 'D'}}
		]
	},
	{
		firstName: 1, 
		lastName: 1,
		_id: 0
	}
);

//2nd part
//--------------------------------------------------------

db.users.find(
	{	
		$and: [
			{department: 'HR'},
			{age: {$gte: 70}}

		]
	}
);


//3rd part
//-------------------------------------------------------------------

db.users.find(
	{	
		$and: [
		{firstName: {$regex: 'e'}},
		{age: {$lte: 30}},
		]
	}
);

